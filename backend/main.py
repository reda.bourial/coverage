from fastapi import FastAPI, HTTPException, Request
from pydantic import BaseModel
from utils.address_api import address_to_gps
from utils.db import MongoDBConnection
import asyncio

api_root = "/api"
app = FastAPI()

# Create a single instance of MongoDBConnection
db_connection = MongoDBConnection()

async def assess_coverage(address: str) -> dict:
    """
    Assess the coverage for a given address.

    Args:
        address (str): The address for which coverage needs to be assessed.

    Returns:
        dict: A dictionary containing information about antennas.

    Raises:
        HTTPException: If the address is empty or coordinates cannot be determined.
    """
    if not address:
        raise HTTPException(status_code=400, detail="empty address")

    coordinates = await address_to_gps(address)
    if not coordinates:
        raise HTTPException(status_code=400, detail="could not determine coordinates")

    long, lat = coordinates
    antennas = db_connection.get_coverage(long, lat)
    return {"antennas": antennas}

@app.get(api_root + "/get_coverage")
async def get_coverage(address: str = "") -> dict:
    """
    Get coverage information for a single address.

    Args:
        address (str): The address for which coverage needs to be retrieved.

    Returns:
        dict: A dictionary containing information about antennas.

    Raises:
        HTTPException: If there is an error processing the bulk request.
    """
    return await assess_coverage(address)

@app.post(api_root + "/bulk_get_coverage")
async def bulk_get_coverage(request: Request) -> dict:
    """
    Get coverage information for multiple addresses in bulk.

    Args:
        request (Request): The HTTP request containing JSON data with addresses.

    Returns:
        dict: A dictionary containing information about antennas for each address.

    Raises:
        HTTPException: If there is an error processing the bulk request.
    """
    data = await request.json()
    tasks = [assess_coverage(address) for address in data.values()]
    results = await asyncio.gather(*tasks)
    return dict(zip(data.keys(), results))
