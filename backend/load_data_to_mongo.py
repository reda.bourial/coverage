import csv
import os
from multiprocessing import Pool
from utils.db import MongoDBConnection
from utils.lambert93 import lambert93_to_gps
from utils.address_api import gps_to_address
from progress.bar import Bar


def convert_to_document(obj):
    """
    Convert antenna data to MongoDB document format.

    Args:
        obj (dict): Dictionary containing antenna information.

    Returns:
        dict: MongoDB document format containing location, technology, address, and operator information.
    """
    long, lat = lambert93_to_gps(obj["x"], obj["y"])
    address = gps_to_address(long, lat)
    technologies = ["2G", "3G", "4G"]
    technology = [tech for tech in technologies if obj[tech] == "1"]
    return {
        "location": {"type": "Point", "coordinates": [long, lat]},
        "technology": technology,
        "address": address,
        "operator": obj["Operateur"],
    }


def read_antennas(
    path=os.path.join(os.path.dirname(os.path.abspath(__file__)), "antennas.csv")
):
    """
    Read antenna data from a CSV file.

    Args:
        path (str): Path to the CSV file.

    Returns:
        list: List of dictionaries containing antenna information.
    """
    with open(path, "r") as file:
        csv_reader = csv.DictReader(file)
        return list(csv_reader)


def main():
    """
    Main function to process and insert antenna data into the MongoDB database.
    """
    print("Started processing, go get a beverage: this will take a long time.")

    antennas = read_antennas()
    total_antennas = len(antennas)

    with MongoDBConnection() as db_connection:
        with Pool() as pool, Bar("Processing antennas", max=total_antennas) as bar:
            results = pool.imap_unordered(convert_to_document, antennas)
            results = list(Bar("Processing antennas", max=total_antennas).iter(results))
            print("Processing done, Inserting to the database.")
            db_connection.insert_antennas(results)

    print("All good, 👍 !")


if __name__ == "__main__":
    main()
