from pyproj import Proj, Transformer

# Lambert 93 projection parameters
lambert = Proj(
    "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"
)

# WGS84 projection parameters
wgs84 = Proj("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")

# Transformer object for converting Lambert 93 coordinates to GPS coordinates
transformer = Transformer.from_proj(lambert, wgs84)

def lambert93_to_gps(x, y):
    """
    Convert Lambert 93 coordinates to GPS coordinates.

    Args:
        x (float): Lambert 93 X-coordinate.
        y (float): Lambert 93 Y-coordinate.

    Returns:
        tuple: Tuple containing latitude and longitude in WGS84 coordinate system.
    """
    return transformer.transform(x, y)
