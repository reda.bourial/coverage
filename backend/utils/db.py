import os
from pymongo import MongoClient, GEOSPHERE
from pyproj import Geod
from functools import lru_cache

geod = Geod(ellps="WGS84")


class MongoDBConnection:
    """
    Context manager for MongoDB connection and operations.

    Usage:
    ```
    with MongoDBConnection() as db_connection:
        # Perform MongoDB operations using db_connection
    ```

    The connection details are read from environment variables.
    Ensure that the `MONGODB_ROOT_PASSWORD` variable is set in the environment.

    Attributes:
        client (MongoClient): MongoDB client instance.
        db (Database): MongoDB database instance.
        collection (Collection): MongoDB collection instance for antennas.
    """

    def __init__(self):
        self.client = MongoClient(
            f"mongodb://root:{os.environ.get('MONGODB_ROOT_PASSWORD')}@mongo:27017"
        )
        self.db = self.client["admin"]
        self.collection = self.db["antennas"]

    def __enter__(self):
        """
        Establish MongoDB connection and initialize client, database, and collection.

        Returns:
            MongoDBConnection: Self reference for use within the `with` statement.
        """
        self.__init__()
        return self

    def insert_antennas(self, antennas):
        """
        Insert a list of antennas into the MongoDB collection.

        Args:
            antennas (list): List of antenna documents to be inserted.

        Returns:
            None
        """
        self.collection.insert_many(antennas)
        # add index to speedup lookups
        self.collection.create_index(
            ["operator", "technology", ("location", GEOSPHERE)]
        )

    def get_technology_coverage(
        self, longitude, latitude, operator, technology, radius_km
    ):
        """
        Retrieve technology coverage for a given location, operator, and technology within a specified radius.

        Args:
            longitude (float): Longitude of the target location.
            latitude (float): Latitude of the target location.
            operator (str): Operator name.
            technology (str): Technology name (e.g., "2G", "3G", "4G").
            radius_km (float): Radius in kilometers for coverage search.

        Returns:
            dict: Antenna information for the specified criteria.
        """
        query = {
            "operator": operator,
            "technology": {"$in": [technology]},
            "location": {
                "$near": {
                    "$geometry": {
                        "type": "Point",
                        "coordinates": [longitude, latitude],
                    },
                    "$maxDistance": radius_km * 1000,
                }
            },
        }
        antenna = self.collection.find_one(
            query, {"_id": 0, "operator": 0, "technology": 0}
        )
        if antenna:
            distance = geod.inv(
                longitude,
                latitude,
                antenna["location"]["coordinates"][0],
                antenna["location"]["coordinates"][1],
            )[2]
            antenna["distance"] = int(distance)
            antenna["location"] = antenna["location"]["coordinates"]
        return antenna

    @lru_cache(maxsize=1000)
    def get_coverage(self, longitude, latitude):
        """
        Retrieve coverage information for a given location.

        Args:
            longitude (float): Longitude of the target location.
            latitude (float): Latitude of the target location.

        Returns:
            dict: Coverage information for different operators and technologies.
        """
        technologies_with_radius = [
            ["2G", 30],
            ["3G", 5],
            ["4G", 10],
        ]
        return {
            operator: {
                technology: self.get_technology_coverage(
                    longitude, latitude, operator, technology, radius
                )
                for technology, radius in technologies_with_radius
            }
            for operator in ["Orange", "Bouygues", "SFR", "Free"]
        }

    def __exit__(self, exc_type, exc_value, traceback):
        """
        Close the MongoDB connection when exiting the context.

        Args:
            exc_type: Exception type.
            exc_value: Exception value.
            traceback: Exception traceback.

        Returns:
            None
        """
        self.client.close()
