"""
This module provides unit tests for the Address API functions in address_api module.

Classes:
    TestAddressAPI: Test cases for the Address API functions.

Functions:
    No public functions in this module.
"""

import unittest
from unittest.mock import Mock, patch
import pytest
import requests
import aiohttp
from address_api import gps_to_address, address_to_gps


class TestGpsToAddress(unittest.TestCase):
    """
    Test cases for gps_to_address.

    Methods:
        test_address_to_gps_success: Test the success case of converting an address to GPS coordinates.
        test_address_to_gps_failure: Test the failure case of converting an address to GPS coordinates.  
        test_gps_to_address_success: Test the success case of getting an address from GPS coordinates.
        test_gps_to_address_failure: Test the failure case of getting an address from GPS coordinates.
    """

    @patch('address_api.requests.get')
    def test_gps_to_address_success(self, mock_requests_get):
        """
        Test the success case of getting an address from GPS coordinates.

        Args:
            mock_requests_get (Mock): Mock object for the requests.get function.
        """
        mock_response = Mock()
        mock_response.raise_for_status.return_value = None
        mock_response.json.return_value = {"features": [{"properties": {"label": "Mock Address"}}]}
        mock_requests_get.return_value = mock_response

        result = gps_to_address(0.0, 0.0)

        self.assertEqual(result, "Mock Address")

    def test_gps_to_address_failure(self):
        """
        Test the failure case of getting an address from GPS coordinates.
        """
        with patch('requests.get', side_effect=requests.RequestException()):
            address = gps_to_address(1.0, 2.0, max_retries=3)
        self.assertIsNone(address)

    async def test_address_to_gps_success(self):
        """
        Test the success case of converting an address to GPS coordinates.
        """
        # Mocking the response for successful request
        mock_response = Mock()
        mock_response.json.return_value = {"features": [{"geometry": {"coordinates": [3.0, 4.0]}}]}
        with patch('aiohttp.ClientSession.get', return_value=mock_response):
            coordinates = await address_to_gps("Mocked Address")
        self.assertEqual(coordinates, [3.0, 4.0])
    
    async def test_address_to_gps_failure(self):
        """
        Test the failure case of converting an address to GPS coordinates.
        """
        # Mocking the response for a failed request
        with patch('aiohttp.ClientSession.get', side_effect=aiohttp.ClientError()):
            coordinates = await address_to_gps("Mocked Address")
        self.assertIsNone(coordinates)


if __name__ == '__main__':
    unittest.main()
