import unittest
from unittest.mock import patch, MagicMock
from pymongo import MongoClient, GEOSPHERE
from pyproj import Proj, Geod
import os

from db import MongoDBConnection


class TestMongoDBConnection(unittest.TestCase):
    """
    Test cases for the MongoDBConnection class.
    """

    @classmethod
    def setUpClass(cls):
        """
        Set up the MongoDBConnection class for testing.
        """
        cls.connection = MongoDBConnection()
        cls.connection.client = MagicMock(spec=MongoClient)
        cls.connection.db = cls.connection.client["test_db"]
        cls.connection.collection = cls.connection.db["test_collection"]
        cls.connection.collection.insert_many = MagicMock()

    @classmethod
    def tearDownClass(cls):
        """
        Tear down the MongoDBConnection class after testing.
        """
        cls.connection.client.drop_database("test_db")

    def test_insert_antennas(self):
        """
        Test the insertion of antennas into the MongoDB collection.
        """
        antennas = [
            {
                "operator": "Orange",
                "technology": "4G",
                "location": {"type": "Point", "coordinates": [1, 1]},
            },
            {
                "operator": "Bouygues",
                "technology": "3G",
                "location": {"type": "Point", "coordinates": [2, 2]},
            },
            {
                "operator": "SFR",
                "technology": "2G",
                "location": {"type": "Point", "coordinates": [3, 3]},
            },
        ]
        self.connection.insert_antennas(antennas)
        self.connection.collection.insert_many.assert_called_once_with(antennas)
        self.connection.collection.create_index.assert_called_once_with(
            ["operator", "technology",("location", GEOSPHERE)]
        )

    def test_get_technology_coverage(self):
        """
        Test the retrieval of technology coverage from MongoDB.
        """
        longitude = 1
        latitude = 1
        operator = "Orange"
        technology = "4G"
        radius_km = 10

        antenna = {
            "operator": operator,
            "technology": technology,
            "location": {"type": "Point", "coordinates": [1, 1]},
        }

        self.connection.collection.find_one.return_value = antenna
        result = self.connection.get_technology_coverage(
            longitude, latitude, operator, technology, radius_km
        )

        self.assertEqual(result, antenna)

    def test_get_technology_coverage_no_antenna(self):
        """
        Test the case where no antenna is found for the specified technology coverage.
        """
        longitude = 1
        latitude = 1
        operator = "Orange"
        technology = "4G"
        radius_km = 10

        self.connection.collection.find_one.return_value = None
        result = self.connection.get_technology_coverage(
            longitude, latitude, operator, technology, radius_km
        )

        self.assertIsNone(result)

    def test_get_technology_coverage_with_distance(self):
        """
        Test the retrieval of technology coverage with distance information.
        """
        longitude = 1
        latitude = 1
        operator = "Orange"
        technology = "4G"
        radius_km = 10

        self.connection.collection.find_one.return_value = {
            "location": {
                "type": "Point",
                "coordinates": [-4.777731877992547, 48.35470076931587],
            },
            "technology": ["2G", "3G"],
            "address": "9 Rue Radio Conquet 29217 Le Conquet",
            "operator": "Orange",
        }
        result = self.connection.get_technology_coverage(
            longitude, latitude, operator, technology, radius_km
        )

        self.assertEqual(
            result,
            {
                "location": [-4.777731877992547, 48.35470076931587],
                "technology": ["2G", "3G"],
                "address": "9 Rue Radio Conquet 29217 Le Conquet",
                "operator": "Orange",
                "distance": 5276670,
            },
        )

    def test_get_coverage(self):
        """
        Test the retrieval of coverage information for multiple operators and technologies.
        """
        longitude = 1
        latitude = 1
        expected_result = {
            "Orange": {
                "2G": "get_technology_coverage_return",
                "3G": "get_technology_coverage_return",
                "4G": "get_technology_coverage_return",
            },
            "Bouygues": {
                "2G": "get_technology_coverage_return",
                "3G": "get_technology_coverage_return",
                "4G": "get_technology_coverage_return",
            },
            "SFR": {
                "2G": "get_technology_coverage_return",
                "3G": "get_technology_coverage_return",
                "4G": "get_technology_coverage_return",
            },
            "Free": {
                "2G": "get_technology_coverage_return",
                "3G": "get_technology_coverage_return",
                "4G": "get_technology_coverage_return",
            },
        }
        with patch.object(
            self.connection,
            "get_technology_coverage",
            return_value={
                "operator": "Orange",
                "technology": "2G",
                "location": {"type": "Point", "coordinates": [1, 1]},
            },
        ):
            self.connection.get_technology_coverage.return_value = (
                "get_technology_coverage_return"
            )
            result = self.connection.get_coverage(longitude, latitude)

        self.assertEqual(result, expected_result)

    def test_exit(self):
        """
        Test the exit method to close the MongoDB connection.
        """
        self.connection.__exit__(None, None, None)
        self.connection.client.close.assert_called_once()


if __name__ == "__main__":
    unittest.main()
