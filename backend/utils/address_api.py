import requests
import aiohttp
import time
from async_lru import alru_cache

def gps_to_address(long, lat, max_retries=50):
    """
    Converts geographical coordinates (longitude, latitude) to a human-readable address.

    Parameters:
    - long (float): Longitude of the location.
    - lat (float): Latitude of the location.
    - max_retries (int): Maximum number of retries in case of a failed request. Default is 50.

    Returns:
    - str or None: If successful, returns the human-readable address. Returns None if unsuccessful.
    """
    endpoint = "https://api-adresse.data.gouv.fr/reverse/"
    params = {"lon": long, "lat": lat}

    for attempt in range(max_retries):
        try:
            response = requests.get(endpoint, params=params)
            response.raise_for_status()

            if response.json()["features"]:
                return response.json()["features"][0]["properties"]["label"]
            else:
                return None
        except:
            time.sleep(10)
    return None

@alru_cache(maxsize=1000)
async def address_to_gps(address):
    """
    Converts a human-readable address to geographical coordinates (longitude, latitude) using asynchronous HTTP requests.

    Parameters:
    - address (str): The human-readable address to be converted.

    Returns:
    - list or None: If successful, returns a list containing [longitude, latitude]. Returns None if unsuccessful.
    """
    endpoint = "https://api-adresse.data.gouv.fr/search/"
    params = {"q": address, "limit": 1}

    async with aiohttp.ClientSession() as session:
        async with session.get(endpoint, params=params) as response:
            response_json = await response.json()

    if response_json["features"]:
        coordinates = response_json["features"][0]["geometry"]["coordinates"]
        return [coordinates[0], coordinates[1]]
    return None
