import unittest
from lambert93 import lambert93_to_gps

class TestLambert93ToGPS(unittest.TestCase):
    """
    Test cases for the Lambert93 to GPS conversion function.
    """

    def test_conversion(self):
        """
        Test the conversion of Lambert93 coordinates to GPS coordinates.
        """
        x_lambert93 = 700000
        y_lambert93 = 6600000
        expected_result = (3.0000000000000004, 46.499999999999986)

        result = lambert93_to_gps(x_lambert93, y_lambert93)

        self.assertEqual(result, expected_result)

    def test_negative_coordinates(self):
        """
        Test the conversion of negative Lambert93 coordinates to GPS coordinates.
        """
        x_lambert93 = -500000
        y_lambert93 = -1000000
        expected_result = (-3.9211236651232735, -12.131900317352416)
        result = lambert93_to_gps(x_lambert93, y_lambert93)

        self.assertEqual(result, expected_result)

    def test_zero_coordinates(self):
        """
        Test the conversion of Lambert93 coordinates at the origin to GPS coordinates.
        """
        x_lambert93 = 0
        y_lambert93 = 0
        expected_result = (-1.3630812101178995, -5.983856309209279)

        result = lambert93_to_gps(x_lambert93, y_lambert93)

        self.assertEqual(result, expected_result)

if __name__ == '__main__':
    unittest.main()
