from unittest.mock import patch, MagicMock
import unittest
from fastapi.testclient import TestClient
from main import app, assess_coverage
import asyncio

class TestApp(unittest.TestCase):
    """
    Test cases for the FastAPI app.
    """

    @patch("main.MongoDBConnection")
    @patch("main.address_to_gps")
    def test_get_coverage(self, mock_address_to_gps, mock_MongoDBConnection):
        """
        Test the GET endpoint for retrieving coverage information for a single address.

        Args:
            mock_address_to_gps (MagicMock): Mock for the address_to_gps function.
            mock_MongoDBConnection (MagicMock): Mock for the MongoDBConnection class.
        """
        test_address = "157 boulevard Mac Donald 75019 Paris"
        mock_db_connection = MagicMock()
        mock_MongoDBConnection.return_value.__enter__.return_value = mock_db_connection
        mock_address_to_gps.return_value = (10.0, 20.0)
        mock_db_connection.get_coverage.return_value = {"antennas": [{"info": "example"}]}

        with patch("main.db_connection", mock_db_connection):
            with TestClient(app) as client:
                response = client.get("/api/get_coverage", params={"address": test_address})

        mock_address_to_gps.assert_called_once_with(test_address)
        mock_db_connection.get_coverage.assert_called_once_with(10.0, 20.0)
        assert response.status_code == 200
        assert str(response.json()) == "{'antennas': {'antennas': [{'info': 'example'}]}}"

    @patch("main.MongoDBConnection")
    @patch("main.address_to_gps")
    async def test_bulk_get_coverage(self, mock_address_to_gps, mock_MongoDBConnection):
        """
        Test the POST endpoint for retrieving coverage information for multiple addresses.

        Args:
            mock_address_to_gps (MagicMock): Mock for the address_to_gps function.
            mock_MongoDBConnection (MagicMock): Mock for the MongoDBConnection class.
        """
        test_data = {"address1": "157 boulevard Mac Donald 75019 Paris", "address2": "1 Bd de Parc, 77700 Coupvray"}
        mock_db_connection = MagicMock()
        mock_MongoDBConnection.return_value.__enter__.return_value = mock_db_connection

        mock_assess_coverage = AsyncMock()
        mock_assess_coverage.side_effect = lambda addr: {"antennas": [addr]}
        with patch("main.assess_coverage", mock_assess_coverage):
            with TestClient(app) as client:
                response = await client.post("/api/bulk_get_coverage", json=test_data)

            mock_db_connection.get_coverage.assert_not_called()
            await mock_assess_coverage.assert_has_calls([
                AsyncMock(call_args=("157 boulevard Mac Donald 75019 Paris",)),
                AsyncMock(call_args=("1 Bd de Parc, 77700 Coupvray",))
            ])
            assert response.status_code == 200
            assert response.json() == {
                "address1": {"antennas": ["157 boulevard Mac Donald 75019 Paris"]},
                "address2": {"antennas": ["1 Bd de Parc, 77700 Coupvray"]}
            }


if __name__ == "__main__":
    unittest.main()
