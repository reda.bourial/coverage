import React, { useState } from 'react';
import { Row, Col } from 'antd';
import axios from 'axios';
import SearchBar from './components/SearchBar';
import ResultDisplay from './components/ResultDisplay';

function App() {
  const [loading, setLoading] = useState(false);
  const [results, setResults] = useState(null);

  const handleSearch = async (searchText) => {
    try {
      setLoading(true);

      const response = await axios.get(`/api/get_coverage?address=${searchText}`);
      setResults(response.data);
    } catch (error) {
      alert('An error occurred while fetching data:', error);
      setResults(null);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Row style={{ marginTop: results ? '1%' : '10%' }}>
      <Col span={6} />
      <Col span={12}>
        <SearchBar onSearch={handleSearch} />
        <ResultDisplay loading={loading} results={results} />
      </Col>
    </Row>
  );
}

export default App;
