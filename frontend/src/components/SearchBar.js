import React from 'react';
import { Input, Row } from 'antd';

const { Search } = Input;

const SearchBar = ({ onSearch }) => (
  <Row style={{ marginBottom: "16px" }}>
    <Search
      placeholder="input search text"
      enterButton="Search"
      size="large"
      onSearch={onSearch}
      style={{ width: '100%' }}
    />
  </Row>
);

export default SearchBar;
