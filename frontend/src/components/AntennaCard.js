import React from 'react';
import { Card, Typography } from 'antd';

const { Text } = Typography;

const AntennaCard = ({ provider, technologies }) => (
    <Card title={provider} style={{ marginBottom: '16px', width: "100%" }}>
        {Object.entries(technologies).map(([tech, details]) => (
            <div key={tech}>
                <Text strong>{`${tech} Coverage:`}</Text>
                <ul>
                    {details ? (
                        <>
                            <li>Location: {details.location.join(', ')}</li>
                            <li>Address: {details.address}</li>
                            <li>Distance: {details.distance} meters</li>
                        </>
                    ) : (
                        <li>No coverage available for {tech}</li>
                    )}
                </ul>
            </div>
        ))}
    </Card>
);

export default AntennaCard;
