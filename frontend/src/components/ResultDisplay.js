import React from 'react';
import { Row, Typography } from 'antd';
import AntennaCard from './AntennaCard';

const { Text } = Typography;

const ResultDisplay = ({ loading, results }) => (
  <Row>
    {loading ? (
      <Text>Loading...</Text>
    ) : (
      <>
        {results?.antennas &&
          Object.entries(results.antennas).map(([provider, technologies]) => (
            <AntennaCard key={provider} provider={provider} technologies={technologies} />
          ))}
      </>
    )}
  </Row>
);

export default ResultDisplay;
