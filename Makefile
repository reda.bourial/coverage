up:
	mkdir -p db
	cd frontend && npm install .
	docker-compose up -d
	make import_data
 
refresh:
	docker-compose stop
	docker-compose rm -f
	docker rmi -f $$(docker images -a -q)
	make up

import_data:
	docker exec -it fastapi_app python load_data_to_mongo.py

tail:
	docker logs -f --tail 0 fastapi_app

test:
	docker exec -it fastapi_app pytest
