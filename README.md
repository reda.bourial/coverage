# Quickstart
```
    make up
```
Then go to http://localhost .

# Run unit tests
```
    make test
```
# Live version
You can find the live version [Here](https://coverage.artisandunet.com).